# Rheology of methyl cellulose based solutions

This repository contains data files and analysis tools to recreate the plots 
for Büyükurganci et al. 2022. "Shear rheology of methyl cellulose based buffers
for cell mechanical measurements at high shear rates".

The folder `figures` contains three main folders named `data`, `plots` and `scripts`.

`data` consists of the datafiles, which were used for the analysis.
`scripts` consists of the Jupyter notebooks, that were used to create figures.
`plots` consists of the figure outcomes of the Jupyter notebooks.

To run the notebooks an installation of python>=3.9 is needed.

The `requirements.txt` and `environment.yml` contain all dependencies for the notebooks to work and can be installed
either with pip or conda. Please create an virtual environment, e.g. with Anaconda's `conda create` command and run the following commands:

Create a conda virtual environment from the environment.yml by running:

```
conda env create -f environment.yml
```

The default environment name will be `mc_rheo_py39`

Or from the requirements.txt:


```
# using Conda
conda create --name <env_name> --file requirements.txt

# using pip
pip install -r requirements.txt

```
